﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControls : MonoBehaviour
{
    static Animator anim;
    public float speed = 10.0f;
    public float rotationSpeed = 100.0f;
    bool isRunning = false;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        float translation = Input.GetAxis("Vertical") * speed;
        float rotation = Input.GetAxis("Horizontal") * rotationSpeed;
        translation *= Time.deltaTime;
        rotation *= Time.deltaTime;
        transform.Translate(0, 0, translation);
        transform.Rotate(0, rotation, 0);

        if (translation > 0)
        {
            anim.SetBool("isWalking", true);

            if (isRunning == false)
            {
                speed = 2.0f;
            }
            else speed = 4.0f;

        }
        else if (translation < 0)
        {
            anim.SetBool("isWalkingBackward", true);
            anim.SetBool("isWalking", false);
            speed = 0.5f;
        }
        else
        {
            anim.SetBool("isWalkingBackward", false);
            anim.SetBool("isWalking", false);
        }

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            if (isRunning == false)
            {
                isRunning = true;
                anim.SetBool("canRun", true);
            }
            else
            {
                isRunning = false;
                anim.SetBool("canRun", false);
            }
        }

    }
}
